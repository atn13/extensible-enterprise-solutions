#CRS4369_a4
## An Nguyen

## Inheritence: Person (base class) and Student (derived class)

## Links to Repo
> [a4 repo](https://bitbucket.org/atn13/lis4369_fall15_a4)

## Screenshots
![Screenshot1](images/screenshot1.png?raw=true)
![Screenshot2](images/screenshot2.png?raw=true)

## Link to Website
? [Website](http://atn13.com)