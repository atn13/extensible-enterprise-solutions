#CRS4369_a5
## An Nguyen

## Inheritence: Product (base class), Book (derived class), Software (derived class)

## Links to Repo
> [a5 repo](https://bitbucket.org/atn13/lis4369_fall15_a5)

## Screenshots
![Screenshot1](images/screenshot1.png?raw=true)
![Screenshot2](images/screenshot2.png?raw=true)

## Link to Website
? [Website](http://atn13.com)