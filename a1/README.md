# CRS4369_a1

## An Nguyen

## Links to Repo
> 1. [a1 repo](https://bitbucket.org/atn13/lis4369_fall15_a1)
> 2. [tutorial repo](https://bitbucket.org/atn13/bitbucketstationlocations)
> 3. [teamquotes repo](https://bitbucket.org/atn13/myteamquotes)

## Screenshot
![Screenshot](images/Screen.png?raw=true)

## Git Commands and Description
> 1. git init: creates new local repository with specified name
> 2. git status: lists all new or modified files to be committed
> 3. git add: snapshots the file in preparation in versioning
> 4. git commit: records file snapshots permanently in version history
> 5. git push: uploads all history from the repository bookmark
> 6. git pull: downloads bookmark history and incorporates changes
> 7. git rm: deletes the file from the working directory and stages the deletion

## Link to Web site
> [Website](http://atn13.com)
